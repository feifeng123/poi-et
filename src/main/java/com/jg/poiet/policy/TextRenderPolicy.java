package com.jg.poiet.policy;

import com.jg.poiet.NiceXSSFWorkbook;
import com.jg.poiet.XSSFTemplate;
import com.jg.poiet.template.cell.CellTemplate;
import com.jg.poiet.data.TextRenderData;
import com.jg.poiet.util.StyleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TextRenderPolicy extends AbstractRenderPolicy<Object> {

    @Override
    public void doRender(CellTemplate cellTemplate, Object renderData, XSSFTemplate template) {
        XSSFCell cell = cellTemplate.getCell();
        Helper.renderTextCell(cell, renderData, template);
    }

    public static class Helper {

        public static void renderTextCell(XSSFCell cell, Object renderData,XSSFTemplate template) {
            renderTextCell(cell, renderData, template.getXSSFWorkbook());
        }

        public static void renderTextCell(XSSFCell cell, Object renderData, NiceXSSFWorkbook workbook) {
            if (null == renderData) {
                renderData = new TextRenderData();
            }
            // text
            TextRenderData textRenderData = renderData instanceof TextRenderData
                    ? (TextRenderData) renderData
                    : new TextRenderData(renderData.toString());

            String data = null == textRenderData.getText() ? "" : textRenderData.getText();

            StyleUtils.styleCell(workbook, cell, textRenderData.getStyle());

            if (StringUtils.isEmpty(data)) {
                cell.setCellValue(data);
            } else {
                switch (textRenderData.getDataType()) {
                    case String:
                        cell.setCellValue(data);
                        break;
                    case Double:
                        cell.setCellValue(Double.valueOf(data));
                        break;
                    default:
                        cell.setCellValue(data);
                }
            }
        }

    }

}
